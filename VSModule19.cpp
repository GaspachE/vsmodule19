﻿#include <iostream>
using namespace std;

class Animal
{
public:

	virtual void Voice() {};
};

class Cat : public Animal
{
public:

	void Voice() override
	{
		cout << "Meow\n";
	}
};

class Dog : public Animal
{
public:

	void Voice() override
	{
		cout << "Woof\n";
	}
};

class Bird : public Animal
{
public:

	void Voice() override
	{
		cout << "Tweet\n";
	}
};



int main()
{
	Animal* arr[3];
	arr[0] = new Cat();
	arr[1] = new Dog();
	arr[2] = new Bird();

	for (Animal* a : arr)
	{
		a->Voice();
	}
}